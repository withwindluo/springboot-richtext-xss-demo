package com.luo.beanFilter;


import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
*
* 采用@Bean方式注册过滤器
* @author Zhiguang
* @since 2021/12/19 23:49
*/

@Configuration
public class WebFilterConfig {

    @Bean
    public FilterRegistrationBean firstFilterRegistrationBean(){
        FilterRegistrationBean registrationBean = new FilterRegistrationBean(new FirstFilter());
        registrationBean.addUrlPatterns("/test01/*");
        registrationBean.setName("firstFilter");
        registrationBean.setOrder(1);
        return registrationBean;
    }

    @Bean
    public FilterRegistrationBean secondFilterRegistrationBean(){
        FilterRegistrationBean registrationBean = new FilterRegistrationBean(new SecondFilter());
        registrationBean.addUrlPatterns("/test02/test02");
        registrationBean.setName("secondFilter");
        registrationBean.setOrder(2);
        return registrationBean;
    }
}
