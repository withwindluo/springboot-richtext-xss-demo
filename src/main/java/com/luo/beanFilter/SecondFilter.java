package com.luo.beanFilter;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

/**
*
*
* @author Zhiguang
* @since 2021/12/20 0:07
*/

public class SecondFilter implements Filter {

    Logger log = LoggerFactory.getLogger(SecondFilter.class);

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        log.info("SecondFilter过滤器初始化");
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest httpServletRequest = (HttpServletRequest) servletRequest;
        log.info("SecondFilter执行过滤..." + httpServletRequest.getRequestURI());
        filterChain.doFilter(servletRequest, servletResponse);
        log.info("SecondFilter过滤结束...");
    }

    @Override
    public void destroy() {
        log.info("SecondFilter过滤器销毁");
    }

}
