package com.luo.webFilter;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.annotation.Order;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

/**
 * @author Zhiguang
 * @WebFilter时Servlet3.0新增的注解，原先实现过滤器，需要在web.xml中进行配置，而现在通过此注解，启动启动时会自动扫描自动注册。
 * @WebFilter filterName 定义注册的过滤器的名字
 * urlPatterns 定义要拦截所有的请求
 * @since 2021/12/19 22:44
 */

@WebFilter(filterName = "PersonFilter", urlPatterns = {"/test01/*", "/test02/test01"})
@Order(3)
public class PersonFilter implements Filter {

    Logger log = LoggerFactory.getLogger(PersonFilter.class);

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        log.info("PersonFilter过滤器初始化");
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest httpServletRequest = (HttpServletRequest) servletRequest;
        log.info("PersonFilter执行过滤..." + httpServletRequest.getRequestURI());
        filterChain.doFilter(servletRequest, servletResponse);
        log.info("PersonFilter过滤结束...");
    }

    @Override
    public void destroy() {
        log.info("PersonFilter过滤器销毁");
    }
}
