package com.luo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;


/**
*
* 富文本xss攻击防御过滤，基于jsoup的白名单
* @author Zhiguang
* @since 2021/12/13 2:33
*/

@SpringBootApplication
@ServletComponentScan
public class SpringbootRichtextXssApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringbootRichtextXssApplication.class, args);
    }

}
