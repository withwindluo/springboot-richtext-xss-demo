package com.luo.controller;


import com.luo.entity.ServiceResponse;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
*
*
* @author Zhiguang
* @since 2021/12/13 2:39
*/

@RestController
@Controller
@RequestMapping("/test01")
public class TestController01 {

    @GetMapping("/test01")
    @ResponseBody
    public ServiceResponse<Object> test01(String s){
        return ServiceResponse.createSuccessByData(s);
    }


    @GetMapping("/test02")
    @ResponseBody
    public ServiceResponse<Object> test02(String s){
        return ServiceResponse.createSuccessByData(s);
    }

}
