package com.luo.controller;


import com.luo.entity.Person;
import com.luo.entity.ServiceResponse;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

/**
*
*
* @author Zhiguang
* @since 2021/12/13 2:39
*/

@RestController
@Controller
@RequestMapping("/xss")
public class XssController {


    @PostMapping("/test")
    @ResponseBody
    public ServiceResponse<Object> test(@RequestBody Person person){
        return ServiceResponse.createSuccessByData(person);
    }


}
