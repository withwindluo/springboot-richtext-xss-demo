package com.luo.entity;

import lombok.Data;

import java.io.Serializable;


/**
*
*
* @author Zhiguang
* @since 2021/12/11 10:38
*/

@Data
public class Person implements Serializable {

    private static final long serialVersionUID = 1L;

    public Person() {
    }

    public Person(String name, Integer age, String desc) {
        this.name = name;
        this.age = age;
        this.desc = desc;
    }

    private String name;

    private Integer age;
    
    private String desc;

}
